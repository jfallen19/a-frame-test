# A Frame Test README

In A nutshell:
A-Frame is a simple JS plugin for HTML. By adding two js files, you can create interactive 3D environments that can be controlled by your computer AND your smart phone. It comes with built-in VR systems, so you can actually use your phone's Virtual Reality setup to run it. Because it uses HTML elements, you can (and generally should) attach a Jquery plugin script and you can use animation and JQuery to edit the scenes.
 
Tutorials:
The tutorials are awful. Properties are hidden and things don't work straight off their site, and everything is incomplete and sometimes flat out wrong. So, here's some documentation of how to develop using A-Frame.
In the html <head>, add a script tag for the aframe.min.js file. Also add the Component js file and a jquery js file.
In the <body>, add an <a-scene> tag. Everything in the A-Frame thing goes here, then close it with the </a-scene> tag. Typically, it's best left in full screen, meaning other HTML elements may not work above or below the scene.
You can pre-define assets with <a-assets>, which makes it easier to load and call in the script. You define images or 3d objects you use in the scene one by one. close out with </a-assets>. Note: If you add a "primitive," for example a simple red sphere or a green cube or just a "blue" sky without clouds or textures, you don't need to define it, you can just change the color in the material tag like an HTML attribute (more on that below).
Next, you can add effects to the camera. You do this by calling <a-camera></a-camera>. To attach an item to the camera, simply put it right inside the tags. For example, you can use <a-cursor color:"blue"></a-cursor> to add a little ring cursor to the screen. I do this to create a cursor so the users know they're supposed to mouse over objects.
Next, you add everything to build your scene.
 
IDs: You can define an ID to any object you create using standard HTML - <a-asset-item id="spiral1"> for example. You can select it using jquery.
Models: use <a-asset-item> tags for 3D objects and sounds. It's easiest to use the obj model type. Be sure to attach the "mtl" file as well, which contains the UV coordinates so the system knows how to apply the texture. We can use Blender, a free open source Python-built app, to create models.
Textures: images for texture or for display are added with a simple <img> tag. do this in assets so you can reference them by id.
Useless: the <a-mixin> tag is supposed to be a shorthand thing to quickly reference a long block, for example adding multiple shaders. I don't think it's in use, and if it is, take it out because it's just dumb.
Fuse Clicking: So, you can't click on the phone like you can click in a PC due to how Javascript works. So, they fix this by creating a "Fuse". Setting fuse on will send a "click" event to the object that the camera is looking at for X milliseconds, in my case 200. I would generally keep fuse ON for simple scenes, because phones can't click.
Object properties: Extra properties that go before the closing > tag.
Scale - Determines the size in X (left/right), Y (up/down), and Z (depth). Typically requires some testing to find the right size.
scale="1 5 3"     - 1 wide, 5 tall, and 3 long.
Position - the position in the world.
position="5 0.1 6"     - 5 and 6 far, but barely lifted off the ground at 0.1.
Rotation - rotation added along the multiple axes. If you have a plane, you'll typically have to rotate it so you can see it. Rotating the Y axes will spin it around like an office chair, X will flip it sideways, and Z will flip it longways.
rotation="270 0 0"     - flips an object 3/4ths of the way around so you can see it's underbelly.
Material properties: Defined in material="all material properties go here"
shader - defines how the object is viewed and interacts with light. To make an object have light reflections, just leave this whole property out and it will use a default shader. You could also use "shader: flat" to just give it a single color that's not affected by light at all.
src - the source material. You can reference a shader or texture here. typically, you want to reference a single texture. Note that even if you created a tiled texture object in blender, you should still re-define it using your own texture and not the "spiral-mtl" material. Granted, you should still upload an mtl file to make sure the UV coordinates work okay. So, you can use "src: #imagetexture;" to add a picture to the scene.
repeat - defines how often the picture repeats. A value of 1 1 will only show one picture, while a value of 8 8 will repeat the picture across the model 8 times horizontally and 8 times vertically. Typically if you have a large floor or tiled object, you'll want to add some sort of tiling. use "repeat: 8 16;" 
Basic Shapes: You can define "primitives" using <a-entity> with the property geometry="primitive: nameOfShape".
<a-entity geometry="primitive: plane"></a-entity> - adds a plane. You may need to scale and rotate it, as planes are only seen from one side. Probably shouldn't start with this one
<a-entity geometry="primitive: cube"></a-entity> - cube. Basic.
<a-entity geometry="primitive: sphere"></a-entity> - sphere.
You can also find cones, cylinders, dodecahedrons and whatnot. Check out for the different types of shapes you can add.
OBJ properties: If you have an OBJ model, use the <a-obj-model...> tag to add it. Note that everything you are going to add is a property so nothing goes between the > and </ tags. First, I reference "src" and use the id #spiral to reference the asset I defined above with id="spiral". Next, you create a material, which is how the item is rendered in the world. IMPORTANT: Don't include the closing ; tag on the final property.
Adding Interaction is done as extra properties.
Use the event-set__NameOfMethod thing, using a double underscore, to define and create events. You create your own name for these.
Next, in the quotes, define the DOM event fired. In our case, we have "mouseenter", "mouseleave", and "click". (remember, clicks are fired as well if you have the FUSE thingy set.)
then, you can define the attributes that you want to change. You can use Animation, but it looked like a whole other can of worms, so we'll just leave that.
You can use _target: #idOfTarget to change a different object. I found that it doesn't change both objects at once - you can't run a bunch of events, and then change the target and run events on another object in a single event. I spoofed this in the 360 image by using positioning to bring hide the sphere behind the text object. So, either change the current object, or use _target to change another object, but not both.
_target: #sky; color: #FF0000; (changes the target to an entity called sky, and changes it to red.)